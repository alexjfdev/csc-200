from random import randint
from gasp.utils import read_number

correct = 0
for i in range(10):
    print("That's right - well done." if (ans := read_number(f"What's {(num0 := randint(1,10))} {'plus' if (opnum := randint(0,3)) == 0 else 'minus' if opnum == 1 else 'times' if opnum == 2 else 'divided by'} {(num1 := randint(1,10))}? ")) == (num0 + num1 if opnum == 0 else num0 - num1 if opnum == 1 else num0 * num1 if opnum == 2 else num0 / num1) else f"No, I’m afraid the answer is {num0 * num1}.")
    correct = correct + 1 if ans == (num0 + num1 if opnum == 0 else num0 - num1 if opnum == 1 else num0 * num1 if opnum == 2 else num0 / num1) else correct
print(f"\nI asked you 10 questions. You got {correct} of them right.\nWell done!")
