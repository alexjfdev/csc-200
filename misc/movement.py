from gasp import *



begin_graphics()
x = 320
y = 240
c = Circle((x,y),5, filled = True)

while True:
    key = update_when('key_pressed')

    x = x + 10 if key in ["KP_6","d"] else x - 10 if key in ["KP_4","a"] else x
    y = y + 10 if key in ["KP_8","w"] else y - 10 if key in ["KP_2","s"] else y

    if key == 'q':     # See Sheet C if you don't understand this
        break          # See Sheet L if you aren't sure what this means
    move_to(c, (x,y))
    print([x,y])
end_graphics()
