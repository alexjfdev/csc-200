from random import randint
from gasp.utils import read_number
import time

while(True):
    while (upper:=(read_number("Enter the high number: "))) < 2 or int(upper) != upper:
        print("Please enter a whole number larger than 1")
    print(upper)
    players = 0
    while (players:=input("How many players?")) not in ["1","2"]:
        print("There can only be 1 or 2 players.")
    if players == "2":players = "bot" if input("Would you like to play with a bot: ").lower() in ["yes", "true", "1", "y", "ye", "sure"] else "2"
    guess = 0
    guessCount = 0
    player1 = input("Player 1 what is your name: " if players == "2" else "What is your name: ")
    if players == "2":
        player2 = input("Player 2 what is your name: ")
        input(f"{player1} please look away.")
        num = 0
        while (num := read_number(f"OK {player2}, Pick a number between 1 and {upper}.\n")) not in range(1,upper+1):
            print(f"You did not pick a number between 1 and {upper}")
        print(f"{player1} can look again.\nOk {player1}, {player2} has picked a number between 1 and {upper}")
    elif players == "bot":
        num = 0
        while (num = read_number(f"{player1}, enter a number for the bot to guess.")) not in range(1,upper+1):
            print(f"You did not pick a number between 1 and {upper}")

    else:
        num=randint(1,upper)
        print(f"OK {player1}, I've thought of a number between 1 and {upper}.")
    while guess != num:
        if players == "bot":
            print("")
        guessCount = guessCount + 1
        print(("That was " + ("my number" if players == "1" else f"{player2}\'s number")+f". Well done!\n\nYou took {guessCount} guesses.") if (guess := read_number("Make a guess: ")) == num else "That's too low." if guess < num else "That's too high.")
    if input("Would you like another game? ").lower() in ["no","false","0"]:break
print("OK. Bye!")
