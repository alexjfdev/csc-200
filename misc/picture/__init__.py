from gasp import *
from random import randint
class face:
    def __init__(self, x, y, scale):
        self.x = x
        self.y = y
        self.scale = scale
    def draw(self):
        Circle((self.x, self.y), 40*self.scale)
        Circle((self.x-(15*self.scale), self.y+(10*self.scale)), 5*self.scale)
        Circle((self.x+(15*self.scale), self.y+(10*self.scale)), 5*self.scale)
        Line((self.x, self.y+(10*self.scale)), (self.x-(10*self.scale), self.y-(10*self.scale)))
        Line((self.x+(10*self.scale), self.y-(10*self.scale)), (self.x-(10*self.scale), self.y-(10*self.scale)))
        Arc((self.x, self.y), 30*self.scale, 225, 90)
        Line((self.x, self.y-(40*self.scale)), (self.x, self.y-(140*self.scale)))
        Line((self.x-(40*self.scale), self.y-(80*self.scale)), (self.x+(40*self.scale), self.y-(80*self.scale)))
        Line((self.x, self.y-(140*self.scale)), (self.x-(40*self.scale), self.y-(180*self.scale)))
        Line((self.x, self.y-(140*self.scale)), (self.x+(40*self.scale), self.y-(180*self.scale)))
