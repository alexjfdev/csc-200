"""
class face:
    def __init__(self, x, y, scale, color=color.BLACK):
        self.x = x
        self.y = y
        self.scale = scale
        self.color = color
    def draw(self):
        Circle((self.x, self.y), 40*self.scale, color=self.color, thickness=self.scale)
        Circle((self.x-(15*self.scale), self.y+(10*self.scale)), 5*self.scale, color=self.color, thickness=self.scale)
        Circle((self.x+(15*self.scale), self.y+(10*self.scale)), 5*self.scale, color=self.color, thickness=self.scale)
        Line((self.x, self.y+(10*self.scale)), (self.x-(10*self.scale), self.y-(10*self.scale)), color=self.color, thickness=self.scale)
        Line((self.x+(10*self.scale), self.y-(10*self.scale)), (self.x-(10*self.scale), self.y-(10*self.scale)), color=self.color, thickness=self.scale)
        Arc((self.x, self.y), 30*self.scale, 225, 315, color=self.color, thickness=self.scale)
        Line((self.x, self.y-(40*self.scale)), (self.x, self.y-(140*self.scale)), color=self.color, thickness=self.scale)
        Line((self.x-(40*self.scale), self.y-(80*self.scale)), (self.x+(40*self.scale), self.y-(80*self.scale)), color=self.color, thickness=self.scale)
        Line((self.x, self.y-(140*self.scale)), (self.x-(40*self.scale), self.y-(180*self.scale)), color=self.color, thickness=self.scale)
        Line((self.x, self.y-(140*self.scale)), (self.x+(40*self.scale), self.y-(180*self.scale)), color=self.color, thickness=self.scale)

#if face is not added to your GASP module remove the triple quotes
"""

from gasp import *
from random import randint
import time

colors=[color.ALICEBLUE,color.ANTIQUEWHITE,color.AQUA,color.AZURE,color.BEIGE,color.BISQUE,color.BLACK,color.BLANCHEDALMOND,color.BLUE,color.BLUEVIOLET,color.BROWN,color.BURLYWOOD,color.CADETBLUE,color.CHARTREUSE,color.CHOCOLATE,color.CORAL,color.CORNFLOWERBLUE,color.CORNSILK,color.CRIMSON,color.CYAN,color.DARKBLUE,color.DARKCYAN,color.DARKGOLDENROD,color.DARKGRAY,color.DARKGREEN,color.DARKKHAKI,color.DARKMAGENTA,color.DARKOLIVEGREEN,color.DARKORANGE,color.DARKORCHID,color.DARKRED,color.DARKSALMON,color.DARKSEAGREEN,color.DARKSLATEBLUE,color.DARKSLATEGRAY,color.DARKTURQUOISE,color.DARKVIOLET,color.DEEPPINK,color.DEEPSKYBLUE,color.DIMGRAY,color.DODGERBLUE,color.FIREBRICK,color.FLORALWHITE,color.FORESTGREEN,color.FUCHSIA,color.GAINSBORO,color.GHOSTWHITE,color.GOLD,color.GOLDENROD,color.GRAY,color.GREEN,color.GREENYELLOW,color.HONEYDEW,color.HOTPINK,color.INDIANRED,color.INDIGO,color.IVORY,color.KHAKI,color.LAVENDER,color.LAVENDERBLUSH,color.LAWNGREEN,color.LEMONCHIFFON,color.LIGHTBLUE,color.LIGHTCORAL,color.LIGHTCYAN,color.LIGHTGOLDENRODYELLOW,color.LIGHTGREEN,color.LIGHTGRAY,color.LIGHTPINK,color.LIGHTSALMON,color.LIGHTSEAGREEN,color.LIGHTSKYBLUE,color.LIGHTSLATEGRAY,color.LIGHTSTEELBLUE,color.LIGHTYELLOW,color.LIME,color.LIMEGREEN,color.LINEN,color.MAGENTA,color.MAROON,color.MEDIUMAQUAMARINE,color.MEDIUMBLUE,color.MEDIUMORCHID,color.MEDIUMPURPLE,color.MEDIUMSEAGREEN,color.MEDIUMSLATEBLUE,color.MEDIUMSPRINGGREEN,color.MEDIUMTURQOISE,color.MEDIUMVIOLETRED,color.MIDNIGHTBLUE,color.MINTCREAM,color.MISTYROSE,color.MOCCASIN,color.NAVAJOWHITE,color.NAVY,color.OLDLACE,color.OLIVE,color.OLIVEDRAB,color.ORANGE,color.ORANGERED,color.ORCHID,color.PALEGOLDENROD,color.PALEGREEN,color.PALETURQOISE,color.PALEVIOLETRED,color.PAPAYAWHIP,color.PEACHPUFF,color.PERU,color.PINK,color.PLUM,color.POWDERBLUE,color.PURPLE,color.RED,color.ROSYBROWN,color.ROYALBLUE,color.SADDLEBROWN,color.SALMON,color.SANDYBROWN,color.SEAGREEN,color.SEASHELL,color.SIENNA,color.SILVER,color.SKYBLUE,color.SLATEBLUE,color.SLATEGRAY,color.SNOW,color.SPRINGGREEN,color.STEELBLUE,color.TAN,color.TEAL,color.THISTLE,color.TOMATO,color.TURQUOISE,color.VIOLET,color.WHEAT,color.WHITE,color.WHITESMOKE,color.YELLOW,color.YELLOWGREEN]

begin_graphics(width=200,height=500,background=color.BLACK,title="Man")
while(True):
    for i in range(1,20):
        man1=Face(100,400,i/10,color=color.GHOSTWHITE)
        man1.Draw()
        time.sleep(.1)
