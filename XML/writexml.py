
from xml.dom import minidom
import os
import datetime
from datetime import date
from datetime import datetime

root = minidom.Document()

filename = ("logs/"+str(date.today())+"_log1.xml").replace(" ","_")
filenum = 1
while os.path.exists(filename) == True:
    filenum = filenum + 1
    filename = ("logs/"+str(date.today())+"_log"+str(filenum)+".xml").replace(" ","_")

xml = root.createElement('root')
root.appendChild(xml)
while True:
    attributeValue = input("Enter log message:")
    productChild = root.createElement(("_"+datetime.now().strftime("%H:%M:%S")).replace(":","_"))
    productChild.setAttribute("message", attributeValue)

    xml.appendChild(productChild)

    xml_str = root.toprettyxml(indent ="\t")

    save_path_file = filename
    try:
        with open(save_path_file, "w") as f:
            f.write(xml_str)
    except FileNotFoundError:
        os.mkdir("logs")
        with open(save_path_file, "w") as f:
            f.write(xml_str)
