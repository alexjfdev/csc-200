from gasp import *
from random import randint

class globals:
    key = ""
    map = []
    playerCords = []
    bots = []
    garbage = []
with open(input("What map would you like to open?\n")+".txt") as mapFile:
    textMap = mapFile.read().splitlines()
    for x in range(2,len(textMap)):
        for y in range(0,len(textMap[x])):
            if textMap[x][y] == "z":
                globals.map.append([0,y, int(textMap[1])-x+1])
            elif textMap[x][y] == "p":
                globals.playerCords = [y, int(textMap[1])-x+1]
            elif textMap[x][y] == "b":
                globals.bots.append([y, int(textMap[1])-x+1])

class player:
    def __init__(self, x = int(textMap[0])/2, y = int(textMap[1])/2, radius = 5, color = color.BLACK, filled = True):
        self.x = x
        self.y =y
        self.radius = radius
        self.color = color
        self.filled = filled
        self.player = Circle((self.x*10, self.y*10), self.radius, color=self.color, filled = self.filled)
    def playerUpdate(self):
        #player = Circle((self.x*10, self.y*10), self.radius, color=self.color, filled = self.filled)
        #while True:
        while globals.key in ["space","KP_5"]:
            self.x = randint(1,int(textMap[1])-1)
            self.y = randint(1,int(textMap[0])-1)
            while [0,self.x,self.y] in globals.map or [self.x,self.y] in globals.bots or [self.x,self.y] in globals.garbage:
                self.x = randint(1,int(textMap[1])-1)
                self.y = randint(1,int(textMap[0])-1)
            globals.playerCords = [self.y, self.x]
            move_to(self.player, (self.x*10,self.y*10))
            print([self.y,self.x])
            globals.key = update_when('key_pressed')

        self.x = self.x + 1 if globals.key in ["KP_6","d"] and [0,self.x + 1,self.y] not in globals.map and [self.y,self.x + 1] not in globals.garbage else self.x - 1 if globals.key in ["KP_4","a"] and [0,self.x - 1,self.y] not in globals.map and [self.y,self.x - 1] not in globals.garbage else self.x
        self.y = self.y + 1 if globals.key in ["KP_8","w"] and [0,self.x,self.y + 1] not in globals.map and [self.y + 1,self.x] not in globals.garbage else self.y - 1 if globals.key in ["KP_2","s"] and [0,self.x,self.y - 1] not in globals.map and [self.y - 1,self.x] not in globals.garbage else self.y
        print(globals.garbage)
        globals.playerCords = [self.y, self.x]
        move_to(self.player, (self.x*10,self.y*10))
        print([self.y,self.x])

class bot:
    def __init__(self, botNum, x = int(textMap[0])/2+2, y = int(textMap[1])/2+2, radius = 5, color = color.RED, filled = True):
        self.botNum = botNum
        self.x = x
        self.y =y
        self.radius = radius
        self.color = color
        self.filled = filled
        self.enabled = True
        self.bot = Circle((self.x*10, self.y*10), self.radius, color=self.color, filled = self.filled)
    def botUpdate(self):
        if self.enabled == False:
            move_to(self.bot, (-1000,-1000))
        else:
            if [self.y,self.x] in globals.garbage:
                self.enabled = False
                self.y = -100-len(globals.garbage)
                exec(f"globals.bots[{self.botNum}] = [self.y,self.x]")
            if self.x > globals.playerCords[1] and [0,self.x - 1,self.y] not in globals.map:
                self.x = self.x - 1
            elif self.x < globals.playerCords[1] and [0,self.x + 1,self.y] not in globals.map:
                self.x = self.x + 1
            if self.y > globals.playerCords[0] and [0,self.x,self.y - 1] not in globals.map:
                self.y = self.y - 1
            elif self.y < globals.playerCords[0] and [0,self.x,self.y + 1] not in globals.map:
                self.y = self.y + 1
            move_to(self.bot, (self.x*10,self.y*10))
            exec(f"globals.bots[{self.botNum}] = [self.y,self.x]")
            for i in range (0, len(globals.bots)):
                if (globals.bots[self.botNum] == globals.bots[i] and i != self.botNum):
                    globals.garbage.append([self.y,self.x])
                    Box((10*self.x, 10*self.y), 10, 10, filled = True, color = color.RED)
                    self.enabled = False
                    self.y = -100-len(globals.garbage)
                    exec(f"globals.bots[{self.botNum}] = [self.y,self.x]")


begin_graphics(width = int(textMap[0]) * 10, height = int(textMap[1]) * 10, title = "Robots")

for i in range(0,len(globals.map)):
    if globals.map[i][0] == 0:
        Box((10*globals.map[i][1], 10*globals.map[i][2]), 10, 10,)


p1 = player(x = globals.playerCords[0], y = globals.playerCords[1])
for i in range(0,len(globals.bots)):
    exec(f"bot{i} = bot({i}, x = globals.bots[{i}][0], y = globals.bots[{i}][1])")

while True:
    globals.key = update_when('key_pressed')
    while globals.key not in ["q","space","w","a","s","d","KP_2","KP_4","KP_5","KP_6","KP_8"]:
        globals.key = update_when('key_pressed')
    if globals.key == 'q':
        break
    p1.playerUpdate()
    for i in range(0,len(globals.bots)):
        exec(f"bot{i}.botUpdate()")
    if globals.playerCords in globals.bots:
        print("dead")
        Text("Game Over!!!", (int(textMap[1])*5, int(textMap[0])*5), size=48)
        sleep(3)
        break
end_graphics()
