import json

with open('assignments.json') as json_file:
    data = json.load(json_file)
print("Which number assignment would you like to open?")
for i in range(0,len(data["assignment"].keys())):
    print(str(i+1)+"."+list(data["assignment"].keys())[i])
while True:
    try:
        assignmentNumber = int(input(""))-1
    except ValueError:
        print("You did not enter a whole number")
    else:
        break
assignmentName = list(data["assignment"].keys())[assignmentNumber]
print(assignmentName+"\n---------------------")
for i in range(0,len(data["assignment"][assignmentName]["questions"])):
    if i == len(data["assignment"][assignmentName]["questions"])-1:
        print(str(i+1)+"."+" "+data["assignment"][assignmentName]["questions"][i][str(i+1)])
    else:
        print(str(i+1)+"."+" "+data["assignment"][assignmentName]["questions"][i][str(i+1)]+"\n")
try:
    print("---------------------\nYou scored a "+str(int(data["assignment"][assignmentName]["score"]))+" on this assignment.")
except KeyError:
    print("---------------------\nThis assignment has not been graded.")
except ValueError:
    print("---------------------\nThis assignment has not been graded.")

#for s in data["assignment"].keys():
#    for i in range(0, len(data["assignment"][s]["questions"])):
#        print(data["assignment"][s]["questions"][i])
